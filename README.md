# Frontend Mentor - Age calculator app solution

This is a solution to the [Age calculator app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/age-calculator-app-dF9DFFpj-Q). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
  - [How to run](#how-to-run)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View an age in years, months, and days after submitting a valid date through the form
- Receive validation errors if:
  - Any field is empty when the form is submitted
  - The day number is not between 1-31
  - The month number is not between 1-12
  - The year is in the future
  - The date is invalid e.g. 31/04/1991 (there are 30 days in April)
- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page
- **Bonus**: See the age numbers animate to their final number when the form is submitted

### Screenshot

![](./screenshot.jpg)

Add a screenshot of your solution. The easiest way to do this is to use Firefox to view your project, right-click the page and select "Take a Screenshot". You can choose either a full-height screenshot or a cropped one based on how long the page is. If it's very long, it might be best to crop it.

Alternatively, you can use a tool like [FireShot](https://getfireshot.com/) to take the screenshot. FireShot has a free option, so you don't need to purchase it. 

Then crop/optimize/edit your image however you like, add it to your project, and update the file path in the image above.

### Links

- Solution URL: [https://gitlab.com/aitorjs/age-calculator-app](https://gitlab.com/aitorjs/age-calculator-app)
- Live Site URL: [https://aitorjs.gitlab.io/age-calculator-app/](https://aitorjs.gitlab.io/age-calculator-app/)

### How to run

- App installation:
```bash
git clone https://gitlab.com/aitorjs/age-calculator-app
cd age-calculator-app
npm install / pnpm install / yarn install
```

- For development: ```npm run dev``` / ```pnpm run dev``` / ```yarn dev```
- For testing: ```npm run test``` / ```pnpm run test``` / ```yarn test```

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- JavaScript
- [Gitlab](https://gitlab.com/) - Git fork
- [Vite](https://vitejs.dev/) - Next generation frontend tooling
- [Vitest](https://vitest.dev/) - Unit Testing Framework

### What I learned

- How to center an image:
```css
img {
  display: block;
  margin-left: auto;
  margin-right: auto;
}
```

- If you want to use vite, use addEventListener insted of onWhatever. Using onWhatever directly on HTML, generate errors.

- Continue practising with CSS, flexbox, typescript, vite, javascript, HTML and deployment.
## Author

- Frontend Mentor - [@aitorjs](https://www.frontendmentor.io/profile/aitorjs)
